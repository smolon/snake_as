﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Snake_AS
{
    class HighScore
    {
        static string fromFile;
        List<string> menuItems = new List<string>();
        static string[] namesFromfile = new string[10];
        static int[] scoresFromFile = new int[10];
        static string[] separators = { /*",", ".", "!", "?", ";", ":",*/ " ", "\r\n" };
        //static FileStream fs = new FileStream("HighScores.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
        //static StreamReader sr = new StreamReader("HighScores.txt");
        //static StreamWriter sw = new StreamWriter("HighScores.txt");
        //public static void ReadFromFile()
        //{

        //    fromFile = sr.ReadToEnd();
        //    //sr.Close();
        //    string[] fromFileSplitted = fromFile.Split(separators, StringSplitOptions.RemoveEmptyEntries);
        //    for (int i = 0, j = 0, k = 0; i < fromFileSplitted.Length; i++)
        //    {
        //        if (i % 2 == 0)
        //        {
        //            namesFromfile[j] = fromFileSplitted[i];
        //            j++;
        //        }
        //        if (i % 2 == 1)
        //        {
        //            scoresFromFile[k] = int.Parse(fromFileSplitted[i].ToString());
        //            k++;
        //        }
        //    }
        //    for (int i = 0; i < namesFromfile.Length; i++)
        //    {
        //        Console.WriteLine(namesFromfile[i] + " " + scoresFromFile[i]);
        //    }
        //}
        public static void ReadFromFile()
        {
            using (StreamReader sr = new StreamReader("HighScores.txt"))
            {
                fromFile = sr.ReadToEnd();
            }
            string[] fromFileSplitted = fromFile.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0, j = 0, k = 0; i < fromFileSplitted.Length; i++)
            {
                if (i % 2 == 0)
                {
                    namesFromfile[j] = fromFileSplitted[i];
                    j++;
                }
                if (i % 2 == 1)
                {
                    scoresFromFile[k] = int.Parse(fromFileSplitted[i].ToString());
                    k++;
                }
            }
            DisplayHighScore();
        }
        
        public static int Score = 0;
        public int[] HighScores = new int[10];
        public static void DisplayHighScore()
        {
            for (int i = 0; i < namesFromfile.Length; i++)
            {
                Console.SetCursorPosition(Console.WindowWidth / 2 - 5, Console.WindowHeight / 2 - 10 + i);
                Console.WriteLine(namesFromfile[i] + " " + scoresFromFile[i]);
            }
        }
        public static bool CheckScore()
        {
            //if(Score >= scoresFromFile[9])
            //{
            //    return true;
            //}
            //return false;
            if (Array.Exists(scoresFromFile, element => element.Equals(Score))) return false;
            if (Score < scoresFromFile[9]) return false;
            return true;
        }
        public static int FindNearestValue()
        {
            int dif = -1;            
            int i = 0;
            for(i = 0; i<scoresFromFile.Length; i++)
            {
                if (scoresFromFile[i] - Score >= dif) dif = scoresFromFile[i] - Score;
            }
            return i-1;
        }
        public static void Insert(string name, int index)
        {
            for (int i = 9; i >= index; i--)
            {
                scoresFromFile[i] = scoresFromFile[i - 1];
                namesFromfile[i] = namesFromfile[i - 1];
            }
            namesFromfile[index - 1] = name;
            scoresFromFile[index - 1] = Score;            
        }
        public static void AddToHighScores()
        {
            if (CheckScore() == true)
            {
                int index = 0; /*Array.IndexOf(scoresFromFile, Score);*/
                Console.Write("\n\n\nPodaj swoje imię: ");
                string name = Console.ReadLine();
                //if(index == -1)
                //{
                //    index = FindNearestValue();
                //}
                for(int i = 0; i<scoresFromFile.Length; i++)
                {
                    if(Score >= scoresFromFile[i])
                    {
                        index = i+1;
                        break;
                    }
                }
                Insert(name, index);
                //scoresFromFile[index] = Score;
                //namesFromfile[index] = name;
                //for (int i = 0; i < scoresFromFile.Length - 1; i++)
                //{                    
                //    if (Score >= scoresFromFile[i] && Score <= scoresFromFile[i + 1])
                //    {
                //        scoresFromFile[i] = Score;
                //        namesFromfile[i] = name;
                //    }
                //}
                File.WriteAllText(@"HighScores.txt", "");
                using (StreamWriter sw = new StreamWriter("HighScores.txt"))
                {
                    for (int i = 0; i < scoresFromFile.Length; i++)
                    {
                        if (i == 9)
                        {
                            sw.Write(namesFromfile[i] + " " + scoresFromFile[i]);
                        }
                        else
                        {
                            sw.WriteLine(namesFromfile[i] + " " + scoresFromFile[i]);
                        }
                    }
                }
                Console.Clear();
                DisplayHighScore();
            }
        }
    }
}
