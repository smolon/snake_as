﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_AS
{
    class Menu
    {
        private static int index = 0;

        private static void Main(string[] args)
        {
            Console.SetWindowSize(56, 38);
            Console.Title = "Sssssnake";
            HighScore.ReadFromFile();
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            List<string> menuItems = new List<string>() {
                "Start",
                "How to play",
                "High Scores",
                "Author",
                "Exit"
            };
            Console.Clear();
            Console.CursorVisible = false;
            while (true)
            {

                string selectedMenuItem = drawMenu(menuItems);
                if (selectedMenuItem == "Start")
                {
                    Game.StartGame();
                }
                else if(selectedMenuItem == "How to play")
                {
                    Console.Clear();
                    Console.SetCursorPosition(Console.WindowWidth / 2 - 20, Console.WindowHeight / 2);
                    Console.WriteLine("Steer the snake with your arrow keys");
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Author")
                {
                    Console.Clear();
                    Console.SetCursorPosition(Console.WindowWidth / 2 - 4, Console.WindowHeight / 2);
                    Console.WriteLine("Adam Solon");
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (selectedMenuItem == "High Scores")
                {
                    Console.Clear();
                    HighScore.DisplayHighScore();
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Exit")
                {
                    Environment.Exit(0);
                }
            }
        }

        private static string drawMenu(List<string> items)
        {
            Console.Clear();
            Console.SetCursorPosition(0, Console.WindowHeight / 2 - 10);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@" _____ _____ _____ _____ _____ _____ _____ __ ________ 
/  ___/  ___/  ___/  ___/  ___/  _  /  _  |  |  /   __\
|___  |___  |___  |___  |___  |  |  |  _  |  _ <|   __|
<_____<_____<_____<_____<_____\__|__\__|__|__|__\_____/
                                                       ");
            Console.ResetColor();
            for (int i = 0; i < items.Count; i++)
            {
                Console.SetCursorPosition(Console.WindowWidth / 2 - 5, Console.WindowHeight / 2 - 4 +i);
                if (i == index)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;

                    Console.WriteLine( " > " + items[i] + " < ");
                }
                else
                {
                    Console.WriteLine(items[i]);
                }
                Console.ResetColor();
            }
            Console.SetCursorPosition(Console.WindowWidth / 2, Console.WindowHeight / 2);

            ConsoleKeyInfo ckey = Console.ReadKey();

            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (index == items.Count - 1)
                {
                    index = 0; 
                }
                else { index++; }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (index <= 0)
                {
                    index = items.Count - 1; 
                }
                else { index--; }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return items[index];
            }
            else
            {
                return "";
            }

            Console.Clear();
            return "";
        }
    }
}
